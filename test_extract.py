import re


def generate_triplet(words_list):
    return [" ".join(words_list[i:i+3]) for i in range(len(words_list) - 2)]


def generate_duplet(words_list):
    return [" ".join(words_list[i:i+2]) for i in range(len(words_list) - 1)]


def keyword_extraction(text):
    text = text.lower()
    text = re.sub('[^A-Za-z0-9\s]+', '', text)

    words_form_text = text.split(" ")
    words = list(set(words_form_text) & set(
        [word.replace('\n', '') for word in open("stopword_list_tala.txt", "r").readlines()]))
    for word in words:
        if word in words_form_text:
            text = text.replace(word, "|")
    keywords = [keyword.strip() for keyword in text.split("|") if keyword.strip()]

    triplet_list = []
    duplet_list = []
    singlet_list = []
    for keyword in keywords:
        triplet_list += generate_triplet(keyword.split(" "))
        duplet_list += generate_duplet(keyword.split(" "))
        singlet_list += keyword.split(" ")

    return list(dict.fromkeys(triplet_list + duplet_list + singlet_list))


if __name__ == '__main__':
    # question = "Apa yang dimaksud dengan Jasa Konsultansi?".lower()
    # question = "Apa yang menjadi Lingkup Pengadaan Barang/Jasa menurut Peraturan Presiden Nomor 16 Tahun 2018 Tentang Pengadaan Barang/Jasa?"
    question = "Lingkup pengadaan menurut peraturan Presiden nomor 16 Tahun 2018 berisi tentang apa ?"
    print(keyword_extraction(question))
